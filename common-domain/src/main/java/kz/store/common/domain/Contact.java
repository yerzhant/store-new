package kz.store.common.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kz.store.entity.billing.Store;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class Contact extends BaseEntity {

    @NotEmpty
    private String lastName;

    @NotEmpty
    private String firstName;

    private String surname;

    @Pattern(regexp = "\\+\\d (\\d{3}) \\d{4} \\d{3}")
    private String mobile;

    private String phone;

    @Email
    private String email;

    @NotNull
    @ManyToOne
    private Store store;
}
